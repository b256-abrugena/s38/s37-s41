// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to connect our frontend application
// Allows us to control the application's cross-origin resource sharing settings
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");

// Setup server
// Create an app variable that stores the result of the "express" function that allows us access to different methods that will make the back-end creation easy. 
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection via Mongoose
mongoose.connect("mongodb+srv://admin:admin1234@b256abrugena.rckx5fh.mongodb.net/B256_CourseAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Routes
app.use("/users", userRoutes);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the cloud database`));

// Server listening
app.listen(4000, () => console.log(`API is now online on port 4000`));