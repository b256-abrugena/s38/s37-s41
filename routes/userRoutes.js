const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");

// Route for checking if the user's email address already exists in the database
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Router for user authentication
router.post("/login", (req, res) => {
	
	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))
})


// FOR S38 ACTIVITY
// Router to retrieve the details of a user

router.post("/details", (req, res) => {
	
	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController))
})



module.exports = router;


