// Set up dependencies
const mongoose = require("mongoose");

// Creation of Schema
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},

	email: {
		type: String,
		required: [true, "E-mail is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "courseId is required"]
			},

			enrolledOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
});

// Module exports to the User Model:
module.exports = mongoose.model("User", userSchema);