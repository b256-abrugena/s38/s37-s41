// Setup dependencies
// We only used the mongoose model po since we have to create the schema and model po which are functions under the mongoose module. However po, since we need to setup our server and connect to our database in the index.js file, we use both packages po because mongoose is responsible for connecting to the database and express is responsible for creating and connecting to the server
const mongoose = require("mongoose");

// Create the schema for our courses
const courseSchema = new mongoose.Schema({
	
	name: {
		type: String,

		// Requires the data for this field/property to be included when creating a record.
		// the "true" value defines if the field is required or not, and the second element in the array is the message we will display in the console when the data needed is not present.
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		required: [true, "Course Description is required"]
	}
	price: {
		type: Number,
		required:[true, "Price is required"]
	}
	isActive: {
		type: Boolean,
		default: true
	}
	createdOn{
		type: Date,

		// The "new Date()" is an expression that instantiates a new "date" that stores the current date and time whenever a course is created in our database.  
		default: new Date() 
	},

	// The "enrollees" property/field will be an array of objects containing the userID and the date and time that the user enrolled. 
	enrollees:[
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});
// End of Schema


// model -> Course (singular and capitalized)
// collections -> courses
// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Course", courseSchema);
