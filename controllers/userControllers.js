// Contains all the business logic and functions of our application
const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkIfEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0){

			return true;
		
		}
		else{

			return false;
		
		}
	})
};


// User registration
/*
	BUsiness Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) =>{

		if(err) {

			return false;
		
		}
		else{

			return true;

		}
	})
}


// User authentication
	/*
		Business Logic:
		1. Check the database if the user email exists
		2. Compare the password provided in the login form with the password stored in the database
		3. Generate/return a JSON web token if the user is successfully logged in and return false if not
	*/

module.exports.authenticateUser = (requestBody) => {
	
	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null){

			return false;

		}
		else{

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password,
				result.password)

			// If the passwords match/result of the above code is true
			if(isPasswordCorrect){

				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects	
				return {access: auth.createAccessToken(result)}
			}

			// Passwords do not match
			else{

				return false;
			}
		}
	})
}


// FOR S38 ACTIVITY
// Controller to retrieve the details of a user

module.exports.getProfile = (requestBody) => {

	return User.find({id: requestBody.id}).then(result => {

		result.password = "";

		return result;
		
	})
};